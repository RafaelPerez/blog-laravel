<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Homepage POST</title>
  </head>
  <body>
    <!-- Logo -->
   <nav class="navbar navbar-light bg-main">
       <div class="container p-4">
           <a class="navbar-brand m-auto" href="#">

           </a>
       </div>
   </nav>
    <!-- Contenido -->
   <section class="container-fluid content">
     @yield('content')
   </section>

    <!-- Footer -->
   <footer class="container-fluid bg-main">

   </footer>
  </body>
</html>
