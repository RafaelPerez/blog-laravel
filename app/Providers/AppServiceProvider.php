<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
//Si la version de mysql es menor 5.7 o mariadb 10.2.2
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
     public function boot()
     {
       Schema::defaultStringLength(191);
     }
}
